﻿using System.Collections.Generic;

namespace MarsRover.Console
{
    class AppStart
    {
        static void Main(string[] args)
        {
            const string plateauParam = "5 5";
            const string rover1PositionParam = "1 2 N";
            const string rover1InstructionsParam = "LMLMLMLMM";

            const string rover2PositionParam = "3 3 E";
            const string rover2InstructionsParam = "MMRMMRMRRM";

            IPlateau plateau = new Plateau(plateauParam);
            IRover rover = new Rover(rover1PositionParam, rover1InstructionsParam);
            IRover rover2 = new Rover(rover2PositionParam, rover2InstructionsParam);
            List<IRover> rovers = new List<IRover>() { rover, rover2 };

            plateau.RunRovers(rovers);
            foreach (IRover item in rovers)
            {
                System.Console.WriteLine(item.ToString());
            }

            System.Console.ReadLine();

        }
    }
}
