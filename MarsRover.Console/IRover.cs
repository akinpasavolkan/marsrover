﻿using System;

namespace MarsRover.Console
{
    public interface IRover
    {
        Guid Id { get; set; }
        Position Position { get; set; }
        char[] Instructions { get; set; }
        void Run();
    }
}
