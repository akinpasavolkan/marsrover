﻿namespace MarsRover.Console
{
    public class ConstVariables
    {
        public const char Left = 'L';
        public const char Right = 'R';
        public const char Move = 'M';

        public const char North = 'N';
        public const char East = 'E';
        public const char South = 'S';
        public const char West = 'W';
    }
}