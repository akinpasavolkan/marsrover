﻿using System.Collections.Generic;

namespace MarsRover.Console
{
    public interface IPlateau
    {
        int X { get; set; }
        int Y { get; set; }
        void RunRovers(List<IRover> rovers);
    }
}
