using System;
using System.Collections.Generic;
using System.Linq;

namespace MarsRover.Console
{
    public class Plateau : IPlateau
    {
        public int X { get; set; }
        public int Y { get; set; }


        public Plateau(int x, int y)
        {
            X = x;
            Y = x;
        }

        public Plateau(string param)
        {
            if (param != null)
            {
                int[] plateauParams = Array.ConvertAll<string, int>(param.Split(' ').ToArray(), int.Parse);
                X = plateauParams[0];
                Y = plateauParams[1];
            }
        }

        public void RunRovers(List<IRover> rovers)
        {
            foreach (var rover in rovers)
            {
                rover.Run();
            }
        }
    }
}