﻿using System;
using System.Linq;

namespace MarsRover.Console
{
    public class Rover : IRover
    {
        public Guid Id { get; set; }
        public Position Position { get; set; }
        public char[] Instructions { get; set; }

        public Rover(string positions, string instructions)
        {
            string[] plateauParams = positions.Split(' ');

            Position = new Position
            {
                X = int.Parse(plateauParams[0]),
                Y = int.Parse(plateauParams[1]),
                Orientation = char.Parse(plateauParams[2])
            };

            Instructions = instructions.ToCharArray();
        }

        public Rover(Position position, char[] instructions)
        {
            Position = position;
            Instructions = instructions;
        }

        public void Run()
        {
            foreach (char instruction in Instructions)
            {
                switch (instruction)
                {
                    case ConstVariables.Left:
                        Left();
                        break;
                    case ConstVariables.Right:
                        Right();
                        break;
                    case ConstVariables.Move:
                        Move();
                        break;


                }
            }


        }

        private void Move()
        {

            switch (Position.Orientation)
            {
                case ConstVariables.North:
                    Position.Y += 1;
                    break;
                case ConstVariables.East:
                    Position.X += 1;
                    break;
                case ConstVariables.South:
                    Position.Y -= 1;
                    break;
                case ConstVariables.West:
                    Position.X -= 1;
                    break;
            }
        }

        private void Right()
        {
            switch (Position.Orientation)
            {
                case ConstVariables.North:
                    Position.Orientation = ConstVariables.East;
                    break;
                case ConstVariables.East:
                    Position.Orientation = ConstVariables.South;
                    break;
                case ConstVariables.South:
                    Position.Orientation = ConstVariables.West;
                    break;
                case ConstVariables.West:
                    Position.Orientation = ConstVariables.North;
                    break;
            }
        }

        private void Left()
        {
            switch (Position.Orientation)
            {
                case ConstVariables.North:
                    Position.Orientation = ConstVariables.West;
                    break;
                case ConstVariables.East:
                    Position.Orientation = ConstVariables.North;
                    break;
                case ConstVariables.South:
                    Position.Orientation = ConstVariables.East;
                    break;
                case ConstVariables.West:
                    Position.Orientation = ConstVariables.South;
                    break;

            }
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", Position.X, Position.Y, Position.Orientation);
        }
    }
}