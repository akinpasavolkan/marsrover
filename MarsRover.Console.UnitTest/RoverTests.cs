﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarsRover.Console.UnitTest
{
    [TestClass]
    public class RoverTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            const string plateauParam = "5 5";
            Guid rover1IdParam = Guid.NewGuid();
            const string rover1PositionParam = "1 2 N";
            const string rover1InstructionsParam = "LMLMLMLMM";

            Guid rover2IdParam = Guid.NewGuid();
            const string rover2PositionParam = "3 3 E";
            const string rover2InstructionsParam = "MMRMMRMRRM";


            IRover rover = new Rover(rover1PositionParam, rover1InstructionsParam) {Id = rover1IdParam};
            IRover rover2 = new Rover(rover2PositionParam, rover2InstructionsParam) { Id = rover2IdParam };
            List<IRover> rovers = new List<IRover>() { rover, rover2 };

            IPlateau plateau = new Plateau(plateauParam);
            plateau.RunRovers(rovers);

            foreach (IRover item in rovers)
            {
                Assert.IsNotNull(item);
                Assert.IsNotNull(item.Position);
            }

            IRover firstRoverResult = rovers.FirstOrDefault(rover1 => rover1.Id == rover1IdParam);
            IRover secondRoverResult = rovers.FirstOrDefault(rover1 => rover1.Id == rover2IdParam);

            Assert.IsNotNull(firstRoverResult);
            Assert.IsNotNull(secondRoverResult);

            Assert.AreEqual(1, firstRoverResult.Position.X);
            Assert.AreEqual(3, firstRoverResult.Position.Y);
            Assert.AreEqual(ConstVariables.North, firstRoverResult.Position.Orientation);

            Assert.AreEqual(5, secondRoverResult.Position.X);
            Assert.AreEqual(1, secondRoverResult.Position.Y);
            Assert.AreEqual(ConstVariables.East, secondRoverResult.Position.Orientation);
        }
    }
}
